package com.itau.vendamatch.controller;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.vendamatch.model.Anuncio;
import com.itau.vendamatch.model.Oferta;
import com.itau.vendamatch.model.Usuario;
import com.itau.vendamatch.repository.AnuncioRepository;
import com.itau.vendamatch.repository.OfertaRepository;
import com.itau.vendamatch.repository.UsuarioRepository;
import com.itau.vendamatch.service.TokenService;

@Controller
public class OfertaController {
	
	@Autowired
	OfertaRepository ofertaRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	AnuncioRepository anuncioRepository;
	
	@Autowired
	TokenService tokenService;
	
	///Set<Oferta> ofertas = new HashSet<Oferta>();

	@RequestMapping(path="/oferta/{id}", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> inserirOferta(@RequestBody Oferta oferta,
			@PathVariable(value="id") int id, HttpServletRequest request) {
		//ofertas.add(oferta);
		int idUsuario = tokenService.verificar(request.getHeader("Authorization"));
		
		Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
		if(!usuario.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		//Optional<Usuario> usuario = usuarioRepository.findByEmail(email);
		Optional<Anuncio> anuncio = anuncioRepository.findById(id);
		
		if(!usuario.isPresent() || !anuncio.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		oferta = ofertaRepository.save(oferta);
		
		Set<Oferta> ofertas = usuario.get().getOfertas();
		ofertas.add(oferta);
		usuario.get().setOfertas(ofertas);		
		usuarioRepository.save(usuario.get());
		
		ofertas = anuncio.get().getOfertas();
		ofertas.add(oferta);
		anuncio.get().setOfertas(ofertas);
		anuncioRepository.save(anuncio.get());
		
		return ResponseEntity.ok(oferta);
	}
	
	@RequestMapping(path="/ofertas", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getVariasOfertas(){
		return ResponseEntity.ok(ofertaRepository.findAll());
	}
	
	@RequestMapping(path="/ofertas2", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getOfertas(HttpServletRequest request){
		int idUsuario = tokenService.verificar(request.getHeader("Authorization"));
		
		Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
		if(!usuario.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		return ResponseEntity.ok(usuario.get().getOfertas());
	}
	
	@RequestMapping(path="/ofertas3", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getOfertasAnuncios(HttpServletRequest request){
		int idUsuario = tokenService.verificar(request.getHeader("Authorization"));
		
		Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
		if(!usuario.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		Set<Anuncio> anuncios = usuario.get().getAnuncios();
		Set<Oferta> ofertas = new HashSet<Oferta>();
		for(Anuncio anuncio: anuncios) {
			for(Oferta oferta: anuncio.getOfertas()) {
				ofertas.add(oferta);
			}
		}
		return ResponseEntity.ok(ofertas);
	}
}
