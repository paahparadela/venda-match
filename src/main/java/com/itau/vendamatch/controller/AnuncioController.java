package com.itau.vendamatch.controller;

import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.vendamatch.model.Anuncio;
import com.itau.vendamatch.model.Usuario;
import com.itau.vendamatch.repository.AnuncioRepository;
import com.itau.vendamatch.repository.UsuarioRepository;
import com.itau.vendamatch.service.TokenService;

@Controller
public class AnuncioController {

	@Autowired
	AnuncioRepository anuncioRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	TokenService tokenService;
	
	//Set<Anuncio> anuncios = new HashSet<Anuncio>();
	
	@RequestMapping(path="/anuncio", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> inserirAnuncio(@RequestBody Anuncio anuncio, HttpServletRequest request) {
		int id = tokenService.verificar(request.getHeader("Authorization"));
		
		Optional<Usuario> usuario = usuarioRepository.findById(id);
		if(!usuario.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		Set<Anuncio> anuncios = usuario.get().getAnuncios();
		anuncios.add(anuncio);
		anuncio = anuncioRepository.save(anuncio);
		usuario.get().setAnuncios(anuncios);
		usuarioRepository.save(usuario.get());
		
		return ResponseEntity.ok(anuncio);
	}
	
	@RequestMapping(path="/anuncio/{id}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getUsuarioByEmail(@PathVariable int id){
		
		Optional<Anuncio> anuncio = anuncioRepository.findById(id);
		
		if(!anuncio.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(anuncio.get());
	}
	
	@RequestMapping(path="/anuncios", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Anuncio> getVariosAnuncios(){
		return anuncioRepository.findAll();
	}
}