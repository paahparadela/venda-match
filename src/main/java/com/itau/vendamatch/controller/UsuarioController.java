package com.itau.vendamatch.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.vendamatch.model.Usuario;
import com.itau.vendamatch.repository.UsuarioRepository;
import com.itau.vendamatch.service.SenhaService;
import com.itau.vendamatch.service.TokenService;

@Controller
public class UsuarioController {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	SenhaService senhaService;
	
	@Autowired
	TokenService tokenService;
	
	@RequestMapping(path="/usuario", method=RequestMethod.POST)
	@ResponseBody
	public Usuario inserirUsuario(@RequestBody Usuario usuario) {
		String hash = senhaService.encode(usuario.getSenha());
		usuario.setSenha(hash);
		return usuarioRepository.save(usuario);
	}
	
	@RequestMapping(path="/usuario/{email}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getUsuarioByEmail(@PathVariable String email){
		Optional<Usuario> usuario = usuarioRepository.findByEmail(email);
		if(!usuario.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(usuario.get());
	}
	
	@RequestMapping(path="/usuarios", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Usuario> getVariosUsuarios(){
		return usuarioRepository.findAll();
	}
	
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> logar(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBanco = usuarioRepository.findByEmail(usuario.getEmail());	
		
		if(! usuarioBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		boolean userOk = senhaService.verificar(usuario.getSenha(), usuarioBanco.get().getSenha());
		
		if(userOk) {
			String token = tokenService.gerar(usuarioBanco.get().getId());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", token);
			
			
			System.out.println(token);
			
			return new ResponseEntity<Usuario>(usuarioBanco.get(), headers, HttpStatus.OK);

		}
		
		return ResponseEntity.badRequest().build();
	}
	
	@RequestMapping(path="/check", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> verificar(HttpServletRequest request) {
		String token = request.getHeader("Authorization");
		
		System.out.println(token);
		
		long id = tokenService.verificar(token);
		
		Optional<Usuario> usuarioBanco = usuarioRepository.findById((int) id);
		
		if(usuarioBanco.isPresent()) {
			return ResponseEntity.ok(usuarioBanco.get());
		}

		return ResponseEntity.badRequest().build();
	}

}
