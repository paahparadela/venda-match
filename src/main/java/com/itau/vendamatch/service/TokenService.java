package com.itau.vendamatch.service;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

@Service
public class TokenService {
	String secret = "venda@123";
	
	public String gerar(int id) {
		try {
		    Algorithm algorithm = Algorithm.HMAC256(secret);
		    return JWT.create().withClaim("idUsuario", id).sign(algorithm);
		} catch (Exception exception){
			exception.printStackTrace();
			
			return null;
		}
	}
	
	
	public int verificar(String token) {
		try {
		    Algorithm algorithm = Algorithm.HMAC256(secret);
		    JWTVerifier verifier = JWT.require(algorithm).build();
		    DecodedJWT jwt = verifier.verify(token);
		    return jwt.getClaim("idUsuario").asInt();
		} catch (Exception exception){
		    exception.printStackTrace();
			
			return 0;
		}
	}
}