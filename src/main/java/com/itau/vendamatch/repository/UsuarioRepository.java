package com.itau.vendamatch.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.vendamatch.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{

	public Optional<Usuario> findByEmail(String email);
}
