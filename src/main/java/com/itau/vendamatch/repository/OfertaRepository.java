package com.itau.vendamatch.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.vendamatch.model.Oferta;

public interface OfertaRepository extends CrudRepository<Oferta, Integer>{

}
