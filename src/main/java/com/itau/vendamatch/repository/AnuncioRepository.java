package com.itau.vendamatch.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.vendamatch.model.Anuncio;


public interface AnuncioRepository extends CrudRepository<Anuncio, Integer>{
	
	public Optional<Anuncio> findById(int id);

}
